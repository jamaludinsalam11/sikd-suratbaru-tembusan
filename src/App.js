import React,{useEffect, useState} from 'react'
import { StylesProvider, createGenerateClassName } from '@material-ui/core/styles'
import axios from 'axios'
import TembusanBaruAll from './components/TembusanBaruAll'
const generateClassName = createGenerateClassName({
    productionPrefix: 'suratbaru-tembusan',
  });
export default function App(props){
    return(
        <StylesProvider generateClassName={generateClassName}>
            <TembusanBaruAll/>
         </StylesProvider>
    )
}